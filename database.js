/* database.js
 * Author: Kyle Feng
 * Description: A simple database that only uses JSON files
 * For learning purposes only.
 * This library only uses only synchronous (blocking) operations for readability and usability,
 * and is therefore extremely slow.
 * Additionally, the index and records are not cached, but read from disk on every operation.
 * This makes it easier to edit the JSON files by hand if desired.
 * Lazy deletion is used. To clean up deleted records, use the prune() method.
 *
 * Public API:
 * createRecord(id, record): Create a new record
 * getRecord(id): Get a record by ID
 * updateRecord(id, record): Update existing record
 * deleteRecord(id): Delete a record, lazily
 * listRecords(fields): List all records, showing only their specified fields
 * prune(): Prune deleted records from the index and file system
 *
 */

const fs = require('fs');
const path = require('path');
const stringify = require("json-stringify-pretty-compact")

/**
 * Usage:
 * const db = new Database('./data');
 * // ... do stuff
 * 
 */
class Database {
    constructor(dataDirectory) {
        dataDirectory = this.dataDirectory = dataDirectory || 'data';
        const dbIndexPath = this.dbIndexPath = path.join(dataDirectory, 'db.json');

        // Check if data directory exists
        if (!fs.existsSync(dataDirectory)) {
            fs.mkdirSync(dataDirectory);
        }
        else {
            // Throw error if not a directory
            if (!fs.lstatSync(dataDirectory).isDirectory()) {
                throw new Error(dataDirectory + 'is not a directory!');
            }
        }

        // Check if database index exists
        if (!fs.existsSync(dbIndexPath)) {
            // Create empty JSON file
            fs.writeFileSync(dbIndexPath, '{}');
        }

        this.checkIntegrity();
    }

    readIndex() {
        const dbIndexFile = fs.readFileSync(this.dbIndexPath);

        try {
            const dbIndex = JSON.parse(dbIndexFile);
            return dbIndex;
        } catch (err) {
            throw new Error('Database index corrupted');
        }
    }

    writeIndex(dbIndex) {
        if (!dbIndex) { throw new Error('dbIndex missing'); }
        const json = stringify(dbIndex);
        fs.writeFileSync(this.dbIndexPath, json);
    }

    getRecordPath(id) {
        return path.join(this.dataDirectory, `record_${id}.json`);
    }

    checkExists(id) {
        if (!id) { throw new Error('id missing'); }

        const dbIndex = this.readIndex();
        return dbIndex[id] && !dbIndex[id]._deleted;
    }

    listRecords(fields) {
        const dbIndex = this.readIndex();

        // Filter records that have not been deleted
        let ids = Object.keys(dbIndex);
        ids = ids.filter(id => !dbIndex[id]._deleted);

        if (fields) {
					// Return a map of id -> compacted record, showing only the specified fields
					const map = {};
					for (let id of ids) {
						let record = this.getRecord(id);
						map[id] = {};
						for (let field of fields) {
							try {
								map[id][field] = record[field];
							}
							catch (err) {
								throw new Error('field does not exist!')
							}

						}
						return map;
					}
        } else {
					// Return just a list
          return ids;
        }
    }

    createRecord(id, record) {
        if (!id) { throw new Error('id missing'); }
        if (!record) { throw new Error('record missing'); }

        // Check if already exists
        // If it was deleted, that's ok
        const dbIndex = this.readIndex();
        if (dbIndex[id] && !dbIndex[id]._deleted) {
            throw new Error(`A record with id ${id} already exists`);
        }

        // Create the record and JSON file
        const recordPath = path.join(this.dataDirectory, `record_${id}.json`);
        const recordJson = stringify(record);
        fs.writeFileSync(recordPath, recordJson);

        // Add an entry in the index
        const entry = makeRecordEntry(id);
        dbIndex[id] = entry;
        this.writeIndex(dbIndex);

        return record;
    }

    getRecord(id) {
        if (!id) { throw new Error('id missing'); }

        // Check if missing or deleted
        const dbIndex = this.readIndex();
        const entry = dbIndex[id];
        if (!entry || entry._deleted) {
            throw new Error(`No record found with id ` + id);
        }

        // Read from the record JSON
        const recordPath = path.join(this.dataDirectory, `record_${id}.json`);
        const recordJson = fs.readFileSync(recordPath);
        const record = JSON.parse(recordJson);

        return record;
    }

    updateRecord(id, record) {
        if (!id) { throw new Error('id missing'); }
        if (!record) { throw new Error('record missing'); }

        // Check if missing or deleted
        const dbIndex = this.readIndex();
        const entry = dbIndex[id];
        if (!entry || entry._deleted) {
            throw new Error(`No record found with id ` + id);
        }

        // Update the record JSON file
        const recordPath = path.join(this.dataDirectory, `record_${id}.json`);
        const recordJson = stringify(record);
        fs.writeFileSync(recordPath, recordJson);

        // Update the index
        dbIndex[id] = record;
        this.writeIndex(dbIndex);

        return record;
    }

    deleteRecord(id) {
        if (!id) { throw new Error('id missing'); }

        // Check if missing or already deleted
        const dbIndex = this.readIndex();
        const entry = dbIndex[id];
        if (!entry) {
            throw new Error(`No record found with id ` + id);
        } else if (entry._deleted) {
            throw new Error('Record is already deleted');
        }

        // Update the index
        dbIndex[id]._deleted = true; // lazy delete
        this.writeIndex(dbIndex);
    }

    /**
     * Remove deleted records from the index and file system
     */
    prune() {
        const dbIndex = this.readIndex();

        // Loop through index and find deleted entries
        for (let id in dbIndex) {
            const entry = dbIndex[id];
            if (entry._deleted) {
                const recordPath = this.getRecordPath(id);
                // Delete from file system (unlink inode)
                fs.unlinkSync(recordPath);

                // Remove property from object
                delete dbIndex[id];
            }
        }

        this.writeIndex(dbIndex);
    }

    checkIntegrity() {
      const dbIndex = this.readIndex();

      const errors = [];

      // Loop through index
      for (let id in dbIndex) {
        const entry = dbIndex[id];
        if (!entry._deleted) {
          const recordPath = this.getRecordPath(id);
          if (!fs.existsSync(recordPath)) {
            errors.push(new Error(`Record ${id} missing`));
          }
        }
      }

      if (errors.length > 0) {
        throw errors; // not sure if this is orthodox
      }
    }
}

function makeRecordEntry(id) {
    if (!id) { throw new Error('id missing'); }

    return {
        _deleted: false
    }
}

module.exports = Database;
