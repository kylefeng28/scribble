// Load libraries
const express = require('express');
const morgan = require('morgan'); // request logger
const bodyParser = require('body-parser'); // parse HTTP POST body

// Configure our app
const app = express();
app.use(morgan('dev')); // log requests to the console
app.use(bodyParser.json()); // parse JSON

// Serve static files
app.use(express.static(__dirname + '/public'));

// Use our routes
app.use(require('./routes'));

// Start our app
const PORT = 8000; // which port to listen on
app.listen(PORT, function() {
    console.log("Listening on http://localhost:" + PORT);
});