const app = new Vue({
	el: '#app',

	data: {
		notes: [],
		currentNote: createEmptyNote(),
		dirty: false,
	},

	mounted: async function() {
		await this.listNotes();
	},

	methods: {

		listNotes: async function() {
			const res = await axios.get('/api/note');
			const notes = res.data;
			this.notes = notes;
			return notes;
		},

		confirmIfUnsaved: function() {
			if (this.dirty) {
				const userConfirmation = confirm("You have unsaved changes. Are you sure you want to continue?");
				return userConfirmation;
			} else return true;
		},

		markDirty: function() {
			this.dirty = true;
		},

		createNewNote: function() {
			this.currentNote = createEmptyNote();
		},

		loadNote: async function(id) {
			if (!this.confirmIfUnsaved()) {
				return;
			}

			try {
				const res = await axios.get(`/api/note/${id}`);
				const note = res.data;
				console.log(`'Loading ${id}'`);
				this.currentNote = note;
				this.dirty = false;
				return note;
			} catch (err) {
				createAlert('warning', 'Could not load note!')
			}

		},

		createNote: async function() {
			if (!this.confirmIfUnsaved()) {
				return;
			}

			let note;
			try {
				const res = await axios.post('/api/note', this.currentNote);
				note = res.data;
				createAlert('success', 'Created!');

				this.dirty = false;

				// Reload view, since note exists now
				await this.loadNote(note._id);
			} catch (err) {
				createAlert('warning', 'Could not create your note!')
			}

			// Refresh list
			await this.listNotes();
		},

		saveNote: async function() {
			try {
				const res = await axios.put(`/api/note/${this.currentNote._id}`, this.currentNote);
				createAlert('success', 'Saved!')
			} catch (err) {
				createAlert('warning', 'Could not save your note!')
			}

			// Refresh list
			await this.listNotes();
		},

		deleteNote: async function() {
			const userConfirmation = confirm('Are you sure you want to delete this note? If so, press OK. If not, press cancel.');

			if (!userConfirmation) {
				return;
			}

			try {
				const res = await axios.delete(`/api/note/${this.currentNote._id}`);
				createAlert('success', 'Deleted!');

				// Clear view
				this.currentNote = createEmptyNote();

			} catch (err) {
				createAlert('warning', 'Could not delete your note!')
			}

			// Refresh list
			await this.listNotes();

		},

	}
})

function createEmptyNote() {
	return {
		_id: null,
		title: 'Untitled',
		text: ''
	}
}

function createAlert(alertType, msg) {
	// Create an alert element
	// https://v4-alpha.getbootstrap.com/components/alerts/#dismissing
	const template = `
		<div class="alert alert-${alertType} alert-dismissible fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			${msg}
		</div>`;
	const el = $(template);

	// Close after 2 seconds
	setTimeout(function() {
		el.alert('close');
	}, 2000);

	// Insert
	$('#message-area').append(el);

	return el;
}
