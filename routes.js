// Load libraries
const express = require('express');
const shortid = require('shortid'); // used to generate short, unique IDs

// Create a new router
const router = express.Router();

// Open our database
const Database = require('./database');
const db = new Database();

router.get('/api/note', function(req, res) {
    const notes = db.listRecords([ '_id', 'title' ]);
    res.json(notes);
});

// POST: Create new note
router.post('/api/note', function(req, res) {
    const note = req.body;

    // Generate a unique ID
    const id = shortid.generate();
    note._id = id;

    // Save to database
    try {
      db.createRecord(id, note);
      res.json(note);
    } catch (err) {
      // Failed
      res.status(500, 'Could not create note');
    }

});

// GET: Get note
router.get('/api/note/:id', function(req, res) {
    const id = req.params['id'];

    try {
      const note = db.getRecord(id);
      res.json(note);
    } catch (err) {
      res.status(404).send(`Could not find note with id ${id}`);
    }
});

// PUT: Update existing note
router.put('/api/note/:id', function(req, res) {
    const id = req.params['id'];
    const note = req.body;

    try {
      db.updateRecord(id, note);
      res.json(note);
    } catch (err) {
      res.status(500).send('Could not update note');
    }
});

// DELETE: Delete note
router.delete('/api/note/:id', function(req, res) {
    const id = req.params['id'];

    try {
      db.deleteRecord(id);
      res.send(200);
    } catch (err) {
      res.status(500).send('Could not delete note');
    }
});

// When `require` is called on this file, return the `router` object
module.exports = router;
