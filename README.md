# scribble
A simple, minimalistic note-sharing app written in Node.js.

Features:
- Very few dependencies
- A simple, JSON-only database

## Installation
1. Clone this repository
2. `cd` into the directory
3. Run `npm install` 
4. Run `node app.js`
5. Go to http://localhost:8000

## Libraries
- Client
  - [Vue.js](https://vuejs.org/)
  - [jQuery](https://jquery.com/)
  - [axios](https://github.com/axios/axios)
  - [Bootstrap](https://getbootstrap.com/)
- Server
  - [Node.js](https://nodejs.org)
  - [Express](https://expressjs.com/)

## Reference
- [Creating a Single Page Todo App with Node and Angular](https://scotch.io/tutorials/creating-a-single-page-todo-app-with-node-and-angular)
- [Bootstrap 4.0 Documentation](https://getbootstrap.com/docs/4.0/)
