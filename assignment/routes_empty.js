// Load libraries
const express = require('express');
const shortid = require('shortid'); // used to generate short, unique IDs

// Create a new router
const router = express.Router();

// Open our database
const Database = require('./database');
const db = new Database();

router.get('/api/note', function(req, res) {
    const notes = db.listRecords([ '_id', 'title' ]);
    res.json(notes);
});

// POST: Create new note
router.post('/api/note', function(req, res) {
	// TODO
	res.status(501).send('Not implemented yet!')
});

// GET: Get note
router.get('/api/note/:id', function(req, res) {
	// TODO
	res.status(501).send('Not implemented yet!')
});

// PUT: Update existing note
router.put('/api/note/:id', function(req, res) {
	// TODO
	res.status(501).send('Not implemented yet!')
});

// DELETE: Delete note
router.delete('/api/note/:id', function(req, res) {
	// TODO
	res.status(501).send('Not implemented yet!')
});

// When `require` is called on this file, return the `router` object
module.exports = router;
